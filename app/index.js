import React, {Component} from 'react';
import {} from 'react-native';
import {Platform } from 'react-native'; 
import {Tabs, Drawer} from './config/router'; 
import {userhandler} from './config/userHandler';

export default class App extends Component{
    constructor(props){
        super(props);
        this.state = {
            signedIn: false,
            checkedSignIn: false
          };
    }

    componentWillMount(){
        userhandler.isSignedIn()
        .then((res)=>{this.setState({signedIn: res, checkedSignIn: true})})
        .catch((err)=>{console.log(err)});
    }

    render(){
        const { checkedSignIn, signedIn } = this.state;
        if (!checkedSignIn) {
            return null;
          }

        return <Drawer /> 

    }
}
