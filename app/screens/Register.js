import React, {Component} from 'react';
import {
    View,
    Text,
    TextInput,
    Image,
    Dimensions,
    StyleSheet,
    ToastAndroid,
    TouchableOpacity,
    ScrollView,
    ActivityIndicator

} from 'react-native';
import {Button} from 'react-native-elements';
import Icon from 'react-native-vector-icons/MaterialIcons';

import background from '../components/images/background.jpg';
import logo from '../components/images/logo.png';

const base = "https://municom.herokuapp.com";


export default class Register extends Component{

    constructor(){
        super();
        
        this.state = {
            email: null,
            password: null,
        }
    }
    onSubmit(){
        console.log('Registering')
    
        let obj ={
            "email": this.state.email,
            "password": this.state.password,
        }
    
        console.log(obj);
        fetch(base + '/api/user/register',
            {
                method: 'POST',
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json'
                },
    
                body: JSON.stringify(obj)
    
            }).then((response)=> {
                console.log('response', response)
                  if(response.status == 200){ 
                        ToastAndroid.show('Successfully Registered!!', ToastAndroid.CENTER, ToastAndroid.LONG);
                        this.props.navigation.navigate('Log');  
                        return response.json(); 
                  }
                  else{ 
                    ToastAndroid.show('User not verified!!//User and/or password is incorrect!!', ToastAndroid.CENTER, ToastAndroid.LONG);
                    this.props.navigation.navigate('Login');  
                    return response.json(); 
                }
                             
            }).then(result=>{
                //return result.json();
                 
            })
    }
    render(){
        return(
            <ScrollView>
            <Image source={background} style={styles.container}>
            <Image source={logo} style={styles.logo} />

            <View style={{margin: 6, marginBottom: 100}}>
            <TextInput 
                    underlineColorAndroid={'transparent'}
                    style={styles.verifyInput}
                    keyboardType={'email-address'}
                    placeholder={'Email address'}
                    placeholderTextColor={'white'}                    
                    onChangeText={(text) =>{this.setState({email: text})}}
            />

            <TextInput 
                    underlineColorAndroid={'transparent'}
                    style={styles.verifyInput}
                    keyboardType={'numeric'}
                    secureTextEntry={true}
                    placeholder={'Password'}
                    placeholderTextColor={'white'}
                    onChangeText={(text) =>{this.setState({password: text})}}
            />
            </View>

            <View syle={styles.spacer}>
            <Button
                raised
                style ={styles.submitButton}    
                icon={{name: 'fingerprint', type: 'material-community'}}
                title='LOGIN'
                backgroundColor={'#08A3C7'}
                borderRadius={9}
                // loading={true}
                onPress={ () => this.onSubmit() }

            />
            <TouchableOpacity 
                
                onPress={ () => this.props.navigation.navigate('Login')}
            >
            <Text style={{textAlign: 'center', color: 'white'}}><Icon name="arrow-forward"/>Have an account? Login</Text>
            </TouchableOpacity>
            </View>
            </Image>
            </ScrollView>
        )
    }
}

const styles = StyleSheet.create({
    container:{
        flex: 1,
        width:Dimensions.get("window").width,
        height:Dimensions.get("window").height
    },
    logo:{
        marginTop:50,
        alignSelf:'center',
        width: (Dimensions.get("window").width - (Dimensions.get("window").width/4)),
        height:150,
        margin:20
        
    },
    verifyInput:{
        margin: 15,
        borderStyle:'solid',
        borderWidth: 2,
        borderRadius: 9,
        borderColor:'#fff',
        textAlign: 'center',
        color: 'white'
        
    },
    spacer:{
        margin: 100
    },
    submitButton:{
        position: 'absolute', 
        top: 0, 
        left: 0, 
        right: 0,
        bottom: 0,
        
    },
})

