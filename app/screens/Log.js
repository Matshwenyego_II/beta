import React, {Component} from 'react';
import {
    View,
    Text,
    Image,
    StyleSheet,
    Dimensions
} from 'react-native';
import {Button} from 'react-native-elements';
import ActionButton from 'react-native-action-button';
import Icon from 'react-native-vector-icons/FontAwesome';
import Slideshow from 'react-native-slideshow';

import log from '../components/images/log.jpg';
import logo from '../components/images/logo.png';


export default class Login extends Component{
    constructor(props) {
        super(props);
    
        this.state = {
          position: 1,
          interval: null,
          dataSource: [
            {
              url: 'http://rwrant.co.za/wp-content/uploads/2013/09/SANRAL-Ad-03.png', 
            }, {
              url: 'http://rwrant.co.za/wp-content/uploads/2013/09/SANRAL-Ad-01.png',
            }, {
              url: 'http://stop-over.co.za/wp-content/uploads/2017/10/Sanral-logo.gif',
            },
          ],
        };
      }
    
      componentWillMount() {
        this.setState({
          interval: setInterval(() => {
            this.setState({
              position: this.state.position === this.state.dataSource.length ? 0 : this.state.position + 1
            });
          }, 2000)
        });
      }
    
      componentWillUnmount() {
        clearInterval(this.state.interval);
      }

    render(){
        return(
            <Image source={log} style={styles.container}>
            <Image source={logo} style={styles.logo}/>

            <View style={{flex:1}}>
            <Slideshow 
                dataSource={this.state.dataSource}
                position={this.state.position}
                onPositionChanged={position => this.setState({ position })}
                height={342}
                overlay={true}
                arrowSize={0}
                containerStyle={{alignSelf: 'center', marginTop: -55, width:Dimensions.get("window").width,}}
            />

            <ActionButton icon={<Icon name="info" size={20} color="#ffffff" />} buttonColor="rgba(231,76,60,1)" offsetX={5} offsetY={5}>
                <ActionButton.Item buttonColor='#558B2F' title="LOG A REQUEST" onPress={() => this.props.navigation.navigate('Request')}>
                <Icon name="commenting-o" style={styles.actionButtonIcon} />
                </ActionButton.Item>
                <ActionButton.Item buttonColor='#F9A825' title="LOG A INCIDENT" onPress={() => this.props.navigation.navigate('Incident')}>
                <Icon name="exclamation-triangle" style={styles.actionButtonIcon} />
                </ActionButton.Item>
                <ActionButton.Item buttonColor='#c62828' title="LOG A COMPLAINT" onPress={() => this.props.navigation.navigate('Complaint')}>
                <Icon name="exclamation" style={styles.actionButtonIcon} />
                </ActionButton.Item>
            </ActionButton>

            </View>
            </Image>
        )    
    }
}

const styles = StyleSheet.create({
    container:{
        flex: 1,
        width:Dimensions.get("window").width,
        height:Dimensions.get("window").height,
        
    },
    logo:{
        marginTop:50,
        margin: 80,
        alignSelf:'center',
        width: (Dimensions.get("window").width - (Dimensions.get("window").width/4)),
        height:150,
        
    },
    spacer:{
        margin: 10,

    },
    submitButton:{
        position: 'absolute', 
        top: 0, 
        left: 0, 
        right: 0,
        bottom: 0,

    },
    actionButtonIcon: {
        fontSize: 20,
        height: 22,
        color: 'white',
      },
})


