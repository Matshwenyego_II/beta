import React, {Component} from 'react';
import {
    View,
    Text,
    Image,
    StyleSheet,
    Dimensions,
    TextInput,
    ScrollView,
    TouchableOpacity,
    ToastAndroid,
    ActivityIndicator
} from 'react-native';
import {Button} from 'react-native-elements';
import ImagePicker from 'react-native-image-picker';
import Icon from 'react-native-vector-icons/MaterialIcons';


import log from '../components/images/log.jpg';
import logo from '../components/images/logo.png';

const base = "https://municom.herokuapp.com";

export default class Complaint extends Component{

    constructor(){
        super();

        this.state = {
            formData: new FormData(),
            user: '5a16968fdd8c3900126a15e8',
            uploadURL: null,
            latitude: null,
            longitude: null,
            error: null,
        }
    }

title = "";
description = "";

onSubmitPressed(){
    console.log('Submitting Complaint');
    if(!this.state.uploadURL){
        Alert.alert('Incomplete Data', 'Upload a media and try again');
    }else{

        this.state.formData.append('title', this.state.title);
        this.state.formData.append('description', this.state.description);
        this.state.formData.append('owner', this.state.user);
        this.state.formData.append('latitude', -26.0125682);
        this.state.formData.append('longitude', 28.1225078);

        console.log(this.state.formData);
        fetch(base + '/api/complaint',{
            method: 'POST',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'multipart/form-data'
            },
            body: this.state.formData
        })
        .then((response)=>{
            console.log(response);
            if(response.status == 201){ 
                ToastAndroid.show('Thank you for making South Africa great again!!', ToastAndroid.TOP, ToastAndroid.LONG);
            }
        }).then((result)=>{
            console.log(result);
            this.props.navigation.navigate('Log');

        })
        .catch((error)=>{
            console.error(error);
            
        })
    }
}
    render(){
        return(
            <ScrollView>
            <Image source={log} style={styles.container}>
            
            <Image source={logo} style={styles.logo}/>
            
            <View style={{margin: 10, marginBottom: 50}}>
            <TextInput 
                    underlineColorAndroid={'transparent'}
                    style={styles.verifyInput}
                    placeholder={'TITLE'}
                    // fontWeight={20}
                    placeholderTextColor={'#08A3C7'}                    
                    onChangeText={(text) =>{this.setState({title: text})}}
            />
            <TouchableOpacity
                onPress={this.attachMedia.bind(this)}
            >
                <Text style={{alignSelf: 'center', color: '#08A3C7'}}><Icon name="file-upload" size={14} style={{color: '#08A3C7'}} /> Attach Media</Text>
            </TouchableOpacity>
            {(()=>{
                switch(this.state.uploadURL){
                    case null: return null;
                    case "": return null;
                    default: 
                    return (
                        <Image source={{uri: this.state.uploadURL}} style={{height: 100, width: "auto"}} />
                    )

                }
            })()}
            <TextInput 
                    underlineColorAndroid={'transparent'}
                    style={styles.verifyInput}
                    placeholder={'DESCRIPTION'}
                    multiline={true}
                    placeholderTextColor={'#08A3C7'}
                    onChangeText={(text) =>{this.setState({description: text})}}
            />
            </View>
            
            <View syle={styles.spacer}>
            <Button
                raised
                style ={styles.submitButton}    
                icon={{name: 'airplane', type: 'material-community'}}
                title='SUBMIT'
                backgroundColor={'#08A3C7'}
                borderRadius={9}
                // loading={true}
                onPress={ () => this.onSubmitPressed() }

            />
            <TouchableOpacity 
                
                onPress={ () => this.props.navigation.navigate('Log')}
            >
            <Text style={{textAlign: 'center', color: 'white'}}><Icon name="arrow-back"/>Back</Text>
            </TouchableOpacity>
            </View> 
            </Image>
            </ScrollView>
        )    
    }

    componentDidMount() {
        navigator.geolocation.getCurrentPosition(
        (position) => {
            this.setState({
            latitude: position.coords.latitude,
            longitude: position.coords.longitude,
            error: null,
            });
        },
        (error) => this.setState({ error: error.message }),
        { enableHighAccuracy: true, timeout: 20000, maximumAge: 1000 },
        );
    }

    attachMedia(){
        this.setState({ uploadURL: '' })
        
          ImagePicker.showImagePicker({title: 'Select Image'}, (response)=>{
            console.log(response);
            if (response.didCancel) {
              this.setState({
                uploadURL: null
              });
            }
            else if (response.error) {
              this.setState({
                uploadURL: null
              });
            }
            else if (response.customButton) {
              this.setState({
                uploadURL: null
              });
            }else{
            const image = {
              uri: response.uri,
              type: response.type,
              name: response.fileName
            }
            this.setState({
              uploadURL: response.uri
            });
            this.state.formData.append('image', image);
          }
          });
    }
}

const styles = StyleSheet.create({
    container:{
        flex: 1,
        width:Dimensions.get("window").width,
        height:Dimensions.get("window").height
    },
    logo:{
        marginTop:50,
        alignSelf:'center',
        width: (Dimensions.get("window").width - (Dimensions.get("window").width/4)),
        height:150,
        
    },
    verifyInput:{
        margin: 10,
        
        borderStyle:'solid',
        borderWidth: 2,
        borderRadius: 9,
        borderColor:'white',
        textAlign: 'center',
        color: '#08A3C7'
        
    },
    spacer:{

    },
    submitButton:{
        position: 'absolute', 
        top: 0, 
        left: 0, 
        right: 0,
        bottom: 0,
    },
})


