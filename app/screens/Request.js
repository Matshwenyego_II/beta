import React, {Component} from 'react';
import {
    View,
    Text,
    Image,
    StyleSheet,
    Dimensions,
    TextInput,
    ScrollView,
    ToastAndroid,
    ActivityIndicator, 
    TouchableOpacity
} from 'react-native';
import {Button} from 'react-native-elements';
import Icon from 'react-native-vector-icons/MaterialIcons';


import log from '../components/images/log.jpg';
import logo from '../components/images/logo.png';

const base = "https://municom.herokuapp.com";

export default class Login extends Component{

    title = "";
    description = "";

        constructor(){
            super();
            
            this.state = {
                formData: new FormData(),
                user: '5a16968fdd8c3900126a15e8',
                latitude: null,
                longitude: null,
                error: null,
            }
        }

    render(){
        return(
            <ScrollView>
            <Image source={log} style={styles.container}>
            
            <Image source={logo} style={styles.logo}/>
            
            <View style={{margin: 10, marginBottom: 140}}>
            <TextInput 
                    underlineColorAndroid={'transparent'}
                    style={styles.verifyInput}
                    placeholder={'TITLE'}
                    // fontWeight={20}
                    placeholderTextColor={'#08A3C7'}                    
                    onChangeText={(text) =>{this.setState({title: text})}}
            />

            <TextInput 
                    underlineColorAndroid={'transparent'}
                    style={styles.verifyInput}
                    placeholder={'DESCRIPTION'}
                    multiline={true}
                    placeholderTextColor={'#08A3C7'}
                    onChangeText={(text) =>{this.setState({description: text})}}
            />
            </View>
            
            <View syle={styles.spacer}>
            <Button
                raised
                style ={styles.submitButton}    
                icon={{name: 'airplane', type: 'material-community'}}
                title='SUBMIT'
                backgroundColor={'#08A3C7'}
                borderRadius={9}
                // loading={true}
                onPress={ () => this.onSubmitPressed() }

            />
                        <TouchableOpacity 
                
                onPress={ () => this.props.navigation.navigate('Log')}
            >
            <Text style={{textAlign: 'center', color: 'white'}}><Icon name="arrow-back"/>Back</Text>
            </TouchableOpacity>
            </View> 
            </Image>
            </ScrollView>
        );    
    }

    onSubmitPressed(){
    console.log('Submitting request')

    let obj = {   
        "title": this.state.title,
        "description": this.state.description,
        "owner": this.state.user,
        "latitude": -26.0125682,
        "longitude": 28.1225078 
    }

    console.log(obj);
    fetch(base + '/api/request',
        {
            method: 'POST',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            },

            body: JSON.stringify(obj)

        }).then((response)=> {
            console.log('response', response)
              if(response.status == 200){ 
                    ToastAndroid.show('Thank you for making South Africa great again!!', ToastAndroid.TOP, ToastAndroid.LONG);  
                    return response.json(); 
              }           
        }).then(result=>{
            //  console.log('result',result)
             this.props.navigation.navigate('Log');
            
        })
    }

    componentDidMount() {
        navigator.geolocation.getCurrentPosition(
        (position) => {
            this.setState({
            latitude: position.coords.latitude,
            longitude: position.coords.longitude,
            error: null,
            });
        },
        (error) => this.setState({ error: error.message }),
        { enableHighAccuracy: true, timeout: 20000, maximumAge: 1000 },
        );
    }
}

const styles = StyleSheet.create({
    container:{
        flex: 1,
        width:Dimensions.get("window").width,
        height:Dimensions.get("window").height
    },
    logo:{
        marginTop:50,
        alignSelf:'center',
        width: (Dimensions.get("window").width - (Dimensions.get("window").width/4)),
        height:150,
        
    },
    verifyInput:{
        margin: 10,
        
        borderStyle:'solid',
        borderWidth: 2,
        borderRadius: 9,
        borderColor:'white',
        textAlign: 'center',
        color: 'white'
        
    },
    spacer:{

    },
    submitButton:{
        position: 'absolute', 
        top: 0, 
        left: 0, 
        right: 0,
        bottom: 0,
    },
})


