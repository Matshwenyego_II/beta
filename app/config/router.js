import React from 'react'; 
import {Platform} from 'react-native'; 
import {StackNavigator, TabNavigator, DrawerNavigator} from 'react-navigation'; 
import Icon from 'react-native-vector-icons/Ionicons'; 

//Screens
import Log from '../screens/Log'; 

import Request from '../screens/Request'; 
import Incident from '../screens/Incident';
import Complaint from '../screens/Complaint';

//Login Stuff
import Login from '../screens/Login';
import Register from '../screens/Register';

//Drawerbutton in the header
import {DrawerButton} from '../components/Header';

//left drawer button customization.
const LeftDrawerButton = ({navigation}) => {
    return <DrawerButton onPress={() => navigation.navigate('DrawerOpen')} />
}

export const LoginStack = StackNavigator({
    Login: {
        screen: Login,
        navigationOptions: (props) => ({
            headerLeft: <LeftDrawerButton  {...props}/> 
        })
    },
},{
    headerMode: 'none'
});

export const RegisterStack = StackNavigator({
    Register: {
        screen: Register,
        navigationOptions: (props) => ({
            headerLeft: <LeftDrawerButton  {...props}/> 
        })
    },
},{
    headerMode: 'none'
});

export const LogStack = StackNavigator({
    Log: {
        screen: Log, 
         navigationOptions: (props) => ({
            headerLeft: <LeftDrawerButton  {...props}/>
        })
    }, 
},{
    headerMode: 'none'
});

export const RequestStack = StackNavigator({
    Request: {
        screen: Request,
        navigationOptions: (props) => ({
            headerLeft: <LeftDrawerButton  {...props}/> 
        })
    },
},
{
    headerMode: 'none'
});

export const IncidentStack = StackNavigator({
    Incident: {
        screen: Incident,
        navigationOptions: (props) => ({
            headerLeft: <LeftDrawerButton  {...props}/> 
        })
    },
},
{
    headerMode: 'none'
});

export const ComplaintStack = StackNavigator({
    Complaint: {
        screen: Complaint,
        navigationOptions: (props) => ({
            headerLeft: <LeftDrawerButton  {...props}/> 
        })
    },
},
{
    headerMode: 'none'
});

export const Tabs = TabNavigator({
    Log: {
        screen: LogStack,
         navigationOptions: {
            tabBarLabel: 'Log',
            tabBarIcon: ({tintColor}) => <Icon name="ios-bookmarks" size={20} color={tintColor} />
        }
    },
},
{ 
    tabBarPosition: 'bottom',
    animationEnabled: true,
    tabBarOptions: {
        activeTintColor: '#ffffff',
        activeBackgroundColor: 'gold',
        labelStyle: {
            fontSize: 12,
        },
        style: {
            backgroundColor: 'black',
        },
        showIcon: true,
        showLabel: false
    }

});

//Drawer navigator implementation. 
export const Drawer = DrawerNavigator({
    Login: {
        screen: LoginStack,
        navigationOption: {
            drawerLabel: 'Login'
        }
    },
    Register: {
        screen: RegisterStack,
        navigationOption: {
            drawerLabel: 'Register'
        }
    },
    Log: {
        screen: LogStack,
        navigationOption: {
            drawerLabel: 'Log'
        }
    },
    Request: {
        screen: RequestStack,
        navigationOption: {
            drawerLabel: 'Request'
        }
    },
    Incident: {
        screen: IncidentStack,
        navigationOption: {
            drawerLabel: 'Incident'
        }
    },
    Complaint: {
        screen: ComplaintStack,
        navigationOption: {
            drawerLabel: 'Complaint'
        }
    },

 
});

