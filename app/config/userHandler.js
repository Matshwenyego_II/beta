import {AsyncStorage} from 'react-native';

class UserHanlder {
  

    async saveUser(user){
        console.log('Into save method', user);
        try{
        await AsyncStorage.setItem('user', JSON.stringify(user));
        }catch(error){
            console.log(error);
        }
    }

    removeUser(){
        AsyncStorage.removeItem('user');
    }

    isSignedIn(){
        return new Promise((resolve, reject) => {
          AsyncStorage.getItem('user')
            .then(res => {
              if (res !== null) {
                resolve(true);
              } else {
                resolve(false);
              }
            })
            .catch(err => reject(err));
        });
      };

    async getUser(){
        try{
        const value = await AsyncStorage.getItem('user');
        if(value !== null){
            return value;
        }
    }catch(error){
        console.log('Error retrieving user', error);
    }
    }
}

export const userhandler = new UserHanlder();